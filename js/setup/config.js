'use strict';

////
///App main configuration file
define({

        // App Name
        name: 'Palazzo Strozzi app',
        // App id
        appid: 'org.palazzostrozzi.app',
        theme: "md",
        //?observable
        language: i18n.langLocal,
        routesList: appRoutes.getRoutes()
        //+observe istances + events callback


});
