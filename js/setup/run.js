/*
define("setup/run",{

    setup: function(config) {
        window.app = new window.Framework7({
            // App Name
            name: config.name,
            // App id
            id: config.appid,
            // App root element
            root: '#app',
            language: config.language,
            theme: config.theme,
            view: {
                pushState: true
            },
            statusbar: {
                enabled: true,
                overlay: true,
                iosOverlaysWebView: true,
                iosTextColor: "white",
                scrollTopOnClick: false
            },
            // Enable swipe panel
            panel: {
                swipe: 'left',
            },
            popup: {
                closeByBackdropClick: false,
            },

            routes: config.routesList,

            // Global app variables (accessible form views as $root.user.firstName)
            //data: function () {
            //    return {
            //        user: {
            //            firstName: 'John',
            //            lastName: 'Doe',
            //        }
            //    };
            //},

        });
    },


    create: function() {
        console.log('into create function...');
        //Really important to have 'views.create()' assigned to a global variable
        console.log(Framework7.device.phonegap);
        if (!Framework7.device.phonegap) {
            console.log('crea view nel desktop');
            window.mainView = app.views.create('.view-main', {
                url: '/'
            });
        }else
        document.addEventListener("deviceready", (event) => {
            console.log('crea view nel device');
            window.mainView = app.views.create('.view-main', {
                url: '/'
            });
            this.afterone();
            console.log('now it\'s gone');
        });
    },

    start: function(config) {

        console.log('verify in apprun');
        console.log(config);

        this.setup(config);
        this.create();

        console.log('after run/create');
        console.log(mainView.router.currentRoute);


        if (!Framework7.device.phonegap)
        {
            // Dom7
            window.$$ = window.Dom7;
            app.router.navigate('/home/');

            console.log('after first navigate');
            console.log(mainView.router.currentRoute);
            ////to move in helpers dir
            ////Template7 Helpers
            Template7.registerHelper('trim', function (text, value) {
                let length = parseInt(value) ? parseInt(value) : 100;
                return (text.length > length) ? text.substring(0, length) + ' [...]' : text;
                //return text;
            });
            Template7.registerHelper('label', function (label) {

                if(typeof label == "string")
                return window.appData.labels[label];

            });
        }

    },

    afterone() {
        // Dom7
        window.$$ = window.Dom7;

        console.log('after first navigate');
        console.log(mainView.router.currentRoute);
        ////to move in helpers dir
        ////Template7 Helpers
        Template7.registerHelper('trim', function (text, value) {
            let length = parseInt(value) ? parseInt(value) : 100;
            return (text.length > length) ? text.substring(0, length) + ' [...]' : text;
            //return text;
        });
        Template7.registerHelper('label', function (label) {

            if (typeof label == "string")
                return window.appData.labels[label];

        });
    },


    reset: function() {
        console.log('app reset/restart invoked');
    }


})
*/
"use strict";
