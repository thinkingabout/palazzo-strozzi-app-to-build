"use strict";

(function (window) {
  /**
   *
   *
   */
  console.log("::::::INITING APP::::::");
  /***
   *
   *
   *
   */
  var init = window.specials.init();

  var dataPromises = window.syncData.init();

  var deviceReadyPromise = new window.Promise(function (resolve, reject) {
    if (Framework7.device.phonegap) {
      document.addEventListener("deviceready", function (event) {
        resolve(true);
      }, false);
    } else resolve(true);
  });

  window.Promise.all([dataPromises.labelsPromise, dataPromises.dataPromise, deviceReadyPromise /*, loadings*/]).then(function (key) {
    console.log(':::::PRE_RUN:::::');

    if (!key[0] || !key[1]) {
      failApp();
      return;
    } else {
      //PRELIMINARY TRANSLATIONS
      i18n.translateMainMenu();
      appRoutes.loadWebViews();
      console.log('::::::LANG_CHECKED:::::');
      runApp();
    }
  });

  function usePreferredTextZoomCallback(boolean) {
    if (boolean && window.MobileAccessibility) {
      if (window.MobileAccessibility) {
        window.MobileAccessibility.usePreferredTextZoom(false);
      }
    } else {
      console.log("Screen reader: OFF");
    }
  }

  function runApp() {
    if (Framework7.device.phonegap && window.MobileAccessibility) {
      MobileAccessibility.usePreferredTextZoom(usePreferredTextZoomCallback);
    }

    /*
    */

    //CATCH BACK BUTTON
    document.addEventListener("backbutton", function (event) {
      console.log('____called Catch back');
      console.log(event); //in console event if empty...
      //event.preventDefault();
      console.log(mainView.router.currentRoute.path);
      //
      if (mainView.router.currentRoute.path == "/check/" || mainView.router.currentRoute.path == "/home/" || mainView.router.currentRoute.name == "Postcard Composer") {
        console.log('blocking /check/ or /home/ router.back');
        //exit from app when page is home
        if (mainView.router.currentRoute.path == "/home/") navigator.app.exitApp();
        //
        if (mainView.router.currentRoute.name == "Postcard Composer") $$('.page[data-page="postcard-composer"]')[0].f7Component.openExitDialog();
      } else mainView.router.back();
      //
    }, false);

    /*
    */

    var appConfig = {
      name: 'Palazzo Strozzi app',
      appid: 'org.palazzostrozzi.app',
      theme: "md",
      language: i18n.langLocal,
      routesList: appRoutes.getRoutes()
    };
    window.app = new window.Framework7({
      // App Name
      name: appConfig.name,
      // App id
      id: appConfig.appid,
      // App root element
      root: '#app',
      language: appConfig.language,
      theme: appConfig.theme,
      view: {
        pushState: true
        //animate:true
      },
      statusbar: {
        enabled: true,
        overlay: false,
        scrollTopOnClick: false,
        iosOverlaysWebView: false,
        //iosBackgroundColor: "#024C74",
        iosBackgroundColor: "#000000",
        iosTextColor: "white",
        //materialBackgroundColor: "#024C74",
        materialBackgroundColor: "#000000"
      },
      popup: {
        closeByBackdropClick: false
      },
      routes: appConfig.routesList
    });

    app.preloader.show();

    // Dom7
    window.$$ = window.Dom7;

    ////Template7 Helpers
    Template7.registerHelper('trim', function (text, value) {
      var length = parseInt(value) ? parseInt(value) : 100;
      return text.length > length ? text.substring(0, length) + ' ...' : text;
      //return text;
    });
    Template7.registerHelper('label', function (label) {
      //if (typeof label != "string")return;
      var local_label = window.appData.labels[label];
      return local_label;
    });

    console.log('over create function...');

    window.mainView = app.views.create('.view-main', {
      url: '/'
    });
    app.preloader.hide();
    console.log('::::::APP_RUNNING:::::');
    //
    document.addEventListener("offline", onAppOffline, false);
    document.addEventListener("online", onAppOnline, false);
    //
    if (!window.isFirstAppLaunch) {
      app.router.navigate('/home/');
      return;
    }

    $$("html").addClass("no-opacity-dialog");
    app.dialog.create({
      backdrop: false,
      title: 'You are welcome to Palazzo Strozzi',
      text: 'Please choose your language',
      buttons: [{
        text: 'Italiano',
        close: true
      }, {
        text: 'English',
        close: true
      }],
      verticalButtons: false,
      animate: true,
      destroyOnClose: true,
      closeByBackdropClick: false,
      onClick: function onClick(dialog, index) {
        if (index == 0) window.i18n.changeLang('IT');else if (index == 1) window.i18n.changeLang('EN');
        app.router.navigate('/home/');
        $$("html").removeClass("no-opacity-dialog");
      }
    }).open();
  } //endrunapp


  function failApp() {
    var appConfig = {
      language: i18n.langLocal
    };
    window.app = new window.Framework7({
      // App Name
      name: 'Palazzo Strozzi app',
      // App id
      id: 'org.palazzostrozzi.app',
      // App root element
      root: '#app',
      language: appConfig.language,
      theme: "md",
      view: {
        pushState: false
      },
      statusbar: {
        enabled: false,
        overlay: false
      },
      routes: [{
        path: "/",
        options: {
          animate: false
        },
        on: {
          pageBeforeIn: function pageBeforeIn(e, page) {
            window.location.reload();
          },
          pageAfterIn: function pageAfterIn(e, page) {
            window.location.reload();
          }
        }
      }, {
        path: "/fail/",
        options: {
          animate: false
        },
        componentUrl: "./resources/pages/fail.html"
      }]

    });

    // Dom7
    window.$$ = window.Dom7;
    window.mainView = app.views.create('.view-main', {
      url: "/"
    });
    console.log('::::::FAILAPP_RUNNING:::::');

    if (!window.isFirstAppLaunch) {
      app.router.navigate('/fail/');
      return;
    }

    $$("html").addClass("no-opacity-dialog");
    app.dialog.create({
      backdrop: false,
      title: 'You are welcome to Palazzo Strozzi',
      text: 'Please choose your language',
      buttons: [{
        text: 'Italiano',
        close: true
      }, {
        text: 'English',
        close: true
      }],
      verticalButtons: false,
      animate: true,
      destroyOnClose: true,
      closeByBackdropClick: false,
      onClick: function onClick(dialog, index) {
        if (index == 0) window.i18n.changeLang('IT');else if (index == 1) window.i18n.changeLang('EN');
        app.router.navigate('/fail/');
        $$("html").removeClass("no-opacity-dialog");
      }
    }).open();
  } //

})(window);
