"use strict";

(function (window) {

    window.i18n = {

        langLocal: "",
        init: function init() {
            // checks for app language, defaults to "it" if not defined
            if (!getLS("app_language")) {
                setLS("app_language", "it");
                this.langLocal = "it";
                window.isFirstAppLaunch = true;
            } else this.langLocal = getLS("app_language");
            // set HTML document lang attribute
            document.querySelector('html').lang = this.langLocal.toUpperCase();
            //
            this.underlineSelector(this.langLocal);
            //
            console.log('::::i18n inited::::');;
        },
        changeLang: function changeLang(lang) {
            setLS("app_language", lang.toLowerCase());
            this.init();
            window.syncData.refresh();
        },
        translateMainMenu: function translateMainMenu() {
            //
            if (window.appData.content.current_shows.list.length == 0) {
                document.querySelector('#mainMenu li:nth-child(1)').classList.add('empty');
            } else {
                document.querySelector('#mainMenu li:nth-child(1)').classList.remove('empty');
            }
            if (window.appData.content.current_shows.list.length > 1) document.querySelector('#mainMenu li:nth-child(1) > a').innerText = window.appData.labels['current_shows'];else {
                document.querySelector('#mainMenu li:nth-child(1) > a').innerText = window.appData.labels['current_shows_single'];
                document.querySelector('#mainMenu li:nth-child(1) > a').href = "/current-shows/0";
            }
            //
            //
            if (window.appData.content.next_shows.list.length == 0) {
                document.querySelector('#mainMenu li:nth-child(2)').classList.add('empty');
            } else {
                document.querySelector('#mainMenu li:nth-child(2)').classList.remove('empty');
            }
            if (window.appData.content.next_shows.list.length > 1) document.querySelector('#mainMenu li:nth-child(2) > a').innerText = window.appData.labels['next_shows'];else {
                document.querySelector('#mainMenu li:nth-child(2) > a').innerText = window.appData.labels['next_shows_single'];
                document.querySelector('#mainMenu li:nth-child(2) > a').href = "/next-show/0";
            }
            //
            //
            if (window.appData.content.next_events.list.length == 0) {
                document.querySelector('#mainMenu li:nth-child(3)').classList.add('empty');
            } else {
                document.querySelector('#mainMenu li:nth-child(3)').classList.remove('empty');
            }
            if (window.appData.content.next_events.list.length > 1) document.querySelector('#mainMenu li:nth-child(3) > a').innerText = window.appData.labels['next_events'];else {
                document.querySelector('#mainMenu li:nth-child(3) > a').innerText = window.appData.labels['next_events_single'];
                document.querySelector('#mainMenu li:nth-child(3) > a').href = "/next-event/0";
            }
            //
            document.querySelector('#infoMenu-title').innerText = window.appData.labels['info_on_menu'];
            document.querySelector('#infoMenu a:nth-child(1)').innerText = window.appData.labels['opening_hours'];
            document.querySelector('#infoMenu a:nth-child(2)').innerText = window.appData.labels['find'];
            document.querySelector('#infoMenu a:nth-child(3)').innerText = window.appData.labels['faq'];
            document.querySelector('#infoMenu a:nth-child(4)').innerText = window.appData.labels['history'];
            document.querySelector('#infoMenu a:nth-child(5)').innerText = window.appData.labels['foundation'];
            document.querySelector('#infoMenu a:nth-child(6)').innerText = window.appData.labels['visit'];
            document.querySelector('#infoMenu a:nth-child(7)').innerText = window.appData.labels['tickets'];
            document.querySelector('#infoMenu a:nth-child(7)').href = window.appData.labels['tickets_link'];
        },
        changeLinks: function changeLinks() {
            //window.appRoutes.reload();
        },
        underlineSelector: function underlineSelector(lang) {
            if (lang == "it" || lang == "IT") {
                document.querySelector('#menuLangWrapper > button:nth-child(1)').classList.add("underline");
                document.querySelector('#menuLangWrapper > button:nth-child(2)').classList.remove("underline");
            } else if (lang == "en" || lang == "EN") {
                document.querySelector('#menuLangWrapper > button:nth-child(2)').classList.add("underline");
                document.querySelector('#menuLangWrapper > button:nth-child(1)').classList.remove("underline");
            }
        }
    };

    //run
    window.i18n.init();

    //index.html menu nthchild change text if != it
    //1
    //2
    //3
    //item-title
    //
})(window);
