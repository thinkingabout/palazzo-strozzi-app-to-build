"use strict";

(function (window) {

    //set item to Local Storage
    window.setLS = function (key, value, object) {
        if (object) window.localStorage.setItem(key, JSON.stringify(value));else window.localStorage.setItem(key, value);
    };
    //get Local Storage item
    window.getLS = function (key, object) {
        if (object) return JSON.parse(window.localStorage.getItem(key));else return window.localStorage.getItem(key);
    };

    window.loadjscssfile = function (filename, filetype) {
        if (filetype == "js") {
            //if filename is a external JavaScript file
            var fileref = document.createElement('script');
            fileref.setAttribute("type", "text/javascript");
            fileref.setAttribute("src", filename);
        } else if (filetype == "css") {
            //if filename is an external CSS file
            var fileref = document.createElement("link");
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("type", "text/css");
            fileref.setAttribute("href", filename);
        }
        if (typeof fileref != "undefined") document.getElementsByTagName("head")[0].appendChild(fileref);
    };

    /*window.imgToSvg = function()
    {
        jQuery('img.svg').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');
              jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');
                  // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }
                  // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');
                  // Replace image with new SVG
                $img.replaceWith($svg);
              }, 'xml');
          });
    }*/
})(window);
