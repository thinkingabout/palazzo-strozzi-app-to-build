"use strict";

(function (window) {

    window.test = {
        desktop: false
    };

    window.appData = {
        labels: "",
        content: ""
    };

    window.resBasePath = {
        //labels: "http://andrewshock.com/strozziapp/contents/labels_",
        labels: "./resources/data/static/labels_",
        //
        //content: "http://andrewshock.com/strozziapp/contents/palazzo-strozzi-app_"
        //content: "./resources/data/static/palazzo-strozzi-app_"
        content: "https://www.palazzostrozzi.org/wp-content/uploads/palazzo-strozzi-app_"
    };

    window.syncData = {
        urls: {
            labels: window.resBasePath.labels + window.i18n.langLocal.toUpperCase() + ".json",
            content: window.resBasePath.content + window.i18n.langLocal.toUpperCase() + ".json"
        },
        init: function init() {
            //
            //window.app.request.setup({cache:false});
            window.Framework7.request.setup({ cache: false });
            //
            var url = this.urls.labels;
            var labelsPromise = new window.Promise(function (resolve, reject) {

                window.Framework7.request.json(url, function (data) {
                    console.log(':::::::::GETTED 1::::::::::');
                    window.appData.labels = data;
                    resolve(true);
                }, function (error) {
                    console.log(':::::::::CONNECTION ERROR::::::::::');
                    console.log(url);
                    console.log(error);
                    resolve(false);
                });
            });
            url = this.urls.content;
            var dataPromise = new window.Promise(function (resolve, reject) {

                window.Framework7.request.json(url, function (data) {
                    console.log(':::::::::GETTED 2::::::::::');
                    window.appData.content = data;
                    resolve(true);
                }, function (error) {
                    console.log(':::::::::CONNECTION ERROR::::::::::');
                    console.log(url);
                    console.log(error);
                    resolve(false);
                });
            });
            return { labelsPromise: labelsPromise, dataPromise: dataPromise };
        },
        refresh: function refresh() {
            try {
                app.dialog.preloader('');
            } catch (e) {};
            this.urls.labels = window.resBasePath.labels + window.i18n.langLocal.toUpperCase() + ".json";
            this.urls.content = window.resBasePath.content + window.i18n.langLocal.toUpperCase() + ".json";
            var dataPromises = this.init();
            window.Promise.all([dataPromises.labelsPromise, dataPromises.dataPromise]).then(function () {
                mainView.router.refreshPage();
                i18n.translateMainMenu();
                i18n.changeLinks();
                console.log(':::APPLYING_NEW_LANG(' + getLS("app_language") + '):::');
                try {
                    app.dialog.close();
                } catch (e) {};
            });
        }
    };

    window.h = {
        'routes': null
    };

    window.specials = {
        unlockedIds: [],
        init: function init() {
            var authorized = getLS("unlockedContents", true);
            if (authorized && authorized.length > 0) this.unlockedIds = getLS("unlockedContents", true);
            console.log(':::specials_inited::::');
        },
        add: function add(id) {
            id = id.toString();
            if (!this.unlockedIds.includes(id) || this.unlockedIds.length == 0) {
                this.unlockedIds.push(id);
                setLS("unlockedContents", this.unlockedIds, true);
                console.log('setted');
            }
        },

        current: {
            id: ""
        },
        open: {
            set: function set(id) {
                id = id.toString();
                console.log('check for set func');
                console.log(window.specials.unlockedIds);
                console.log('id to check ' + id);
                var check = window.specials.unlockedIds.includes(id);
                console.log('checking for set: ' + check);
                return check;
            },
            authorize: function authorize(id, psw) {
                var unlock_code = window.appData.content.current_shows.list[mainView.router.currentRoute.params.showId].special_gallery.unlock_code;
                if (psw == unlock_code) {
                    window.specials.add(id);
                    return true;
                } else return false;
            },
            getSlides: function getSlides() {
                return window.appData.content.current_shows.list[mainView.router.currentRoute.params.showId].special_gallery.list;
            }
        },
        reset: function reset() {
            setLS("unlockedContents", "");this.unlockedIds = "";
        }
    };

    window.postcard = {
        //current:

        //default stroke color
        strokecolor: '#090909',
        colorpalette: ['#090909', '#9B9B9B', '#F1592A', '#FCC011', '#4CAF51', '#37A5DD', '#664A9E', '#EF4338'],

        strokewidth: 15,

        current: {
            name: "",
            cardUrls: []
        },
        open: {
            get: function get(id) {
                return cardUrkls;
            }
        },

        reset: function reset() {
            this.strokecolor = '#FFCE4D';
            this.strokewidth = 15;
            this.current.cardUrls = [];
        }
    };

    window.shareCard = {
        base64: "",
        postcardUrl: ""
    };

    window.lay = {
        templates: [null]
    };

    window.onAppOffline = function () {
        //alert('::::::::APP IS OFFLINE:::::::::::');
        try {
            mainView.router.navigate('/check/');
        } catch (e) {};
    };
    window.onAppOnline = function () {
        //window.alert('::::::::APP IS ONLINE:::::::::::');
        if (mainView.router.currentRoute.path == "/check/") mainView.router.back();
    };
})(window);
