'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function (window) {
    var Globals = function () {
        function Globals() {
            _classCallCheck(this, Globals);
        }

        _createClass(Globals, null, [{
            key: 'checkConnection',
            value: function checkConnection() {

                console.log('checking connection');
                if (!navigator.connection || !navigator.connection.type) {
                    console.warn('Cannot check the internet connection!');
                    return !Framework7.device.phonegap;
                }

                var NETWORK_STATE = navigator.connection.type;

                return NETWORK_STATE != Connection.NONE;
            }
        }, {
            key: 'removeHeaderFromIframeParam',
            get: function get() {
                return "?app=no";
            }
        }]);

        return Globals;
    }();

    window.Globals = Globals;
})(window);
