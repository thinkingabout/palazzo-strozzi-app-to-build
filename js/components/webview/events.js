'use strict';

define({

    pageMounted: function pageMounted(e, page) {
        //console.log('%c[TestComponent] pageMounted', page);
    },
    pageInit: function pageInit(e, page) {
        console.log('%c[TestComponent] pageInit', page);
        console.log(this);

        app.preloader.show();

        var frame = $$('iframe.webview-frame');
        this.startingUrl = frame[0].src;
        //console.log(this);

        frame.on('load', function () {
            if (this.src.includes('palazzostrozzi.org')) console.log('insite');
            app.preloader.hide();
            frame.width($$(window).width());
        });
    },
    pageBeforeIn: function pageBeforeIn(e, page) {
        //console.log('%c[TestComponent] pageBeforeIn', page);

        //If back from check
        if (mainView.router.previousRoute.name == "Connection Check") {

            console.log('IS BACK FROM CHECK');

            var frame = $$('iframe.webview-frame');
            //frame[0].src = "";
            app.preloader.show();
            frame[0].src = this.startingUrl;
            frame.on('load', function () {
                if (this.src.includes('palazzostrozzi.org')) console.log('insite');
                app.preloader.hide();
                //$$(this).off('load');
            });
        }
        //

    },
    pageAfterIn: function pageAfterIn(e, page) {
        //console.log('%c[TestComponent] pageAfterIn', page);
    },
    pageBeforeOut: function pageBeforeOut(e, page) {
        //console.log('%c[TestComponent] pageBeforeOut', page);
    },
    pageAfterOut: function pageAfterOut(e, page) {
        //console.log('%c[TestComponent] pageAfterOut', page);
    },
    pageBeforeRemove: function pageBeforeRemove(e, page) {
        //console.log('%c[TestComponent] pageBeforeRemove', page);
        var frame = $$('iframe.webview-frame');
        frame.off('load');
    }

});
