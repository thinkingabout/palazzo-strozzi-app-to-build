"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function (window) {
        var AbstractPage = function () {
                function AbstractPage(id, options) {
                        _classCallCheck(this, AbstractPage);
                }

                _createClass(AbstractPage, [{
                        key: "mounted",
                        value: function mounted(data) {}
                }, {
                        key: "init",
                        value: function init(data) {}
                }, {
                        key: "reInit",
                        value: function reInit(data) {}
                }, {
                        key: "beforeIn",
                        value: function beforeIn(data) {}
                }, {
                        key: "afterIn",
                        value: function afterIn(data) {}
                }, {
                        key: "beforeOut",
                        value: function beforeOut(data) {}
                }, {
                        key: "afterOut",
                        value: function afterOut(data) {}
                }, {
                        key: "beforeRemove",
                        value: function beforeRemove(data) {}
                }]);

                return AbstractPage;
        }();

        window.AbstractPage = AbstractPage;
})(window);
