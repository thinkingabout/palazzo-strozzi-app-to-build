"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (window) {
    var HomePage = function (_window$AbstractPage) {
        _inherits(HomePage, _window$AbstractPage);

        function HomePage(id, options) {
            _classCallCheck(this, HomePage);

            var _this = _possibleConstructorReturn(this, (HomePage.__proto__ || Object.getPrototypeOf(HomePage)).call(this, id, options));

            _this.flkty = null;
            return _this;
        }

        _createClass(HomePage, [{
            key: "mounted",
            value: function mounted(data) {
                _get(HomePage.prototype.__proto__ || Object.getPrototypeOf(HomePage.prototype), "mounted", this).call(this, data);
            }
        }, {
            key: "init",
            value: function init(data) {

                //flickity
                //this.flkty = new window.Flickity("#homepage__next-shows-carousel", {
                //    // options
                //});

                _get(HomePage.prototype.__proto__ || Object.getPrototypeOf(HomePage.prototype), "init", this).call(this, data);
            }
        }, {
            key: "reInit",
            value: function reInit(data) {
                _get(HomePage.prototype.__proto__ || Object.getPrototypeOf(HomePage.prototype), "reInit", this).call(this, data);
            }
        }, {
            key: "beforeIn",
            value: function beforeIn(data) {
                _get(HomePage.prototype.__proto__ || Object.getPrototypeOf(HomePage.prototype), "beforeIn", this).call(this, data);
            }
        }, {
            key: "afterIn",
            value: function afterIn(data) {
                _get(HomePage.prototype.__proto__ || Object.getPrototypeOf(HomePage.prototype), "afterIn", this).call(this, data);
            }
        }, {
            key: "beforeOut",
            value: function beforeOut(data) {
                _get(HomePage.prototype.__proto__ || Object.getPrototypeOf(HomePage.prototype), "beforeOut", this).call(this, data);
            }
        }, {
            key: "afterOut",
            value: function afterOut(data) {
                _get(HomePage.prototype.__proto__ || Object.getPrototypeOf(HomePage.prototype), "afterOut", this).call(this, data);
            }
        }, {
            key: "beforeRemove",
            value: function beforeRemove(data) {
                _get(HomePage.prototype.__proto__ || Object.getPrototypeOf(HomePage.prototype), "beforeRemove", this).call(this, data);
            }
        }]);

        return HomePage;
    }(window.AbstractPage);

    window.HomePage = HomePage;
})(window);
