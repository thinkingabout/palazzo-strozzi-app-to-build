'use strict';

define({
    //pageMounted: function (e, page) {},
    pageInit: function pageInit(e, page) {
        console.log('%c[TestComponent] pageInit', page);
        //
        //setting palette's first color button to active
        $$('.picker-color-btn:nth-child(1)').addClass('active');
        //
        $$('.picker-color-btn').on('click', function () {
            $$('.picker-color-btn').removeClass('active');
            $$(this).addClass('active');
        });
        //

        var sheetel = $$('.postcards-list');
        window.postcardSheet = app.sheet.create({
            el: sheetel,
            closeByBackdropClick: false
        });
        //
        app.sheet.open('.postcards-list');
        //
        //
        $$('.strokeColor').css("background", window.postcard.strokecolor);
        //
        //preloader
        $$('div.sheet-modal.postcards-list.modal-in .postcard').on('load', function () {
            $$(this).prev().remove();
        });
        //
        window.tmpResizeEvent = window.addEventListener("resize", function () {
            if (mainView.router.currentRoute.name != "Postcard Composer") return;
            console.log('tmpResizeEvent: resizing...');
            $$('.page[data-page="postcard-composer"]')[0].f7Component.fitArtboard();
        });
        //
    },
    pageBeforeIn: function pageBeforeIn(e, page) {
        console.log('%c[TestComponent] pageBeforeIn', page);
        //
        try {
            window.StatusBar.hide();
        } catch (e) {};
        //
        if (mainView.router.previousRoute.name == "Current Show") {
            this.fitArtboard();
            this.initArtboard();
        }
        //
    },
    pageAfterIn: function pageAfterIn(e, page) {
        //console.log('%c[TestComponent] pageAfterIn', page);
        //
        $$('body').css("opacity", "1");
        try {
            screen.orientation.removeEventListener('change', handleNavigate);
        } catch (e) {}
        //
        //
    },
    pageBeforeOut: function pageBeforeOut(e, page) {
        //console.log('%c[TestComponent] pageBeforeOut', page);
        window.tmpResizeEvent = null;
        //
    },
    pageAfterOut: function pageAfterOut(e, page) {
        //console.log('%c[TestComponent] pageAfterOut', page);
        //
        try {
            window.StatusBar.show();
        } catch (e) {};
        //
    },
    pageBeforeRemove: function pageBeforeRemove(e, page) {
        //console.log('%c[TestComponent] pageBeforeRemove', page);
        //
        window.screenChangeEvent = null;
        app.sheet.destroy('.postcards-list');
        $$('.picker-color-btn').off('click');
    }
});
