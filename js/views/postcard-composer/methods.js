'use strict';

define({

  selectPostcard: function selectPostcard(imgUrl) {
    this.$el.find('.artboard-drawer img')[0].src = imgUrl;
    app.sheet.close('.postcards-list');
    if (window.postcardSheet.params.closeByBackdropClick == false) {
      window.postcardSheet.params.closeByBackdropClick = true;
      //this.initDrawer();
    }
  },

  goNext: function goNext() {},

  goBack: function goBack() {
    this.openExitDialog();
  },

  openExitDialog: function openExitDialog() {
    app.dialog.create({
      title: window.appData.labels.postcardComposerExitDialog_title,
      text: window.appData.labels.postcardComposerExitDialog_subtitle,
      buttons: [{
        text: window.appData.labels.postcardComposerExitDialog_btnNo,
        bold: true,
        close: true
      }, {
        text: window.appData.labels.postcardComposerExitDialog_btnYes,
        onClick: function onClick(dialog, e) {
          window.mainView.router.back();
        }
      }],
      verticalButtons: true
    }).open();
  },

  changeStrokeColor: function changeStrokeColor(color) {
    window.postcard.strokecolor = color;
    $$('.strokeColor').css("background", window.postcard.strokecolor);
  },

  initDraggable: function initDraggable() {
    var gridWidth = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 50;

    if (window.Draggable.get(".stroke-slider .drag")) Draggable.get(".stroke-slider .drag").kill();

    var $container = this.$el.find(".stroke-slider")[0];
    var $dragLevel = this.$el.find('#drag_level')[0];
    var maxHeight = $$($container).height();
    var gridRows = 6;
    var gridHeight = maxHeight / gridRows;

    TweenLite.set($container, { height: maxHeight });
    TweenLite.set(".stroke-slider .drag", { height: gridHeight });

    this.$el.find('#drag-coverEl').remove();
    $$("<div/>").attr('id', 'drag-coverEl').css({ position: 'absolute', height: gridHeight + 'px', width: '20px', top: gridHeight * 5 + 10 + 'px', backgroundColor: 'white', zIndex: 500 }).append('<div id="strokeColorBottom" class="strokeColor"></div>').prependTo($container);
    this.$el.find('.strokeColor').css("background", window.postcard.strokecolor);

    Draggable.create(".stroke-slider .drag", {
      bounds: $container,
      edgeResistance: 1,
      type: "y",
      throwProps: true,
      liveSnap: true,
      snap: {
        y: function y(value) {
          //
          var snapPosition = Math.round(value / gridHeight) * gridHeight;
          //
          if (snapPosition < gridHeight / 2) {
            window.postcard.strokewidth = 25;
          } else if (snapPosition < gridHeight + 1) {
            window.postcard.strokewidth = 20;
          } else if (snapPosition < (gridHeight + 1) * 2) {
            window.postcard.strokewidth = 15;
          } else if (snapPosition < (gridHeight + 1) * 3) {
            window.postcard.strokewidth = 10;
          } else if (snapPosition < (gridHeight + 1) * 4) {
            window.postcard.strokewidth = 5;
          } else if (snapPosition < (gridHeight + 1) * 5) {
            window.postcard.strokewidth = 2;
          }
          //
          var levelLimits = snapPosition > maxHeight ? maxHeight : snapPosition;
          levelLimits = levelLimits < -20 ? -20 : levelLimits;

          $$($dragLevel).css({
            "top": levelLimits + 2 + 'px'
          });

          var maxBottom = gridHeight * 5 - 9;
          if (snapPosition > maxBottom) return maxBottom;
          return snapPosition;
        }
      }
    });
  },

  fitArtboard: function fitArtboard() {
    console.log('Resizing/Fitting artboard method');
    //
    this.$el.find('.stroke-slider').css("height", "");
    this.resizeDrawer();
    this.initDraggable();
    //
    var draggable = Draggable.get(".stroke-slider .drag");
    draggable.update(true, true);
    draggable.update(true, true);
    //
    var level = 4;
    switch (window.postcard.strokewidth) {
      case 2:
        level = 5;break;
      case 5:
        level = 4;break;
      case 10:
        level = 3;break;
      case 15:
        level = 2;break;
      case 20:
        level = 1;break;
      case 25:
        level = 0;break;
    }
    //
    var maxHeight = this.$el.find('.stroke-slider').height();
    var gridHeight = maxHeight / 6;
    //
    var dragPosition = gridHeight * level;
    if (level == 5) dragPosition = gridHeight * 5 - 9;
    //
    TweenLite.set(".stroke-slider .drag", { y: dragPosition });
    //
    this.$el.find('#drag_level').css({
      "top": gridHeight * level + 2 + 'px'
    });
  },

  initArtboard: function initArtboard() {
    this.initDrawer();

    this.$el.find('.picker-color-btn').removeClass('active');
    this.$el.find('.picker-color-btn:nth-child(1)').addClass('active');
    this.changeStrokeColor("#090909");

    this.$el.find('.artboard-menu a').addClass('disabled');

    var gridHeight = $$('.stroke-slider').height() / 6;
    TweenLite.set(".stroke-slider .drag", { y: gridHeight * 4 });
    window.postcard.strokewidth = 5;

    this.$el.find('#drag_level').css({
      "top": gridHeight * 4 + 2 + 'px'
    });
  },

  resetArtboard: function resetArtboard() {
    this.initDrawer();

    //this.$el.find('.picker-color-btn').removeClass('active');
    //this.$el.find('.picker-color-btn:nth-child(1)').addClass('active');
    //this.changeStrokeColor("#090909");

    this.$el.find('.artboard-menu a').addClass('disabled');

    //let gridHeight = $$('.stroke-slider').height() / 6;
    //TweenLite.set(".stroke-slider .drag", { y: gridHeight * 4 });
    //window.postcard.strokewidth = 5;

    //this.$el.find('#drag_level').css({
    //  "top": ((gridHeight * 4) + 2) + 'px'
    //});
  },

  initDrawer: function initDrawer() {
    console.log('initDrawer');
    this.resizeDrawer();
    //console.log('1');
    var canvas = this.$el.find("canvas")[0];
    var stage = new createjs.Stage(canvas);
    var drawingCanvas = void 0;
    var oldPt = void 0;
    var oldMidPt = void 0;
    var title = void 0;
    var color = void 0;
    var stroke = void 0;
    var index = 0;

    //canvas = $container.find("canvas")[0];
    //
    //

    stage.autoClear = false;
    //stage.enableDOMEvents(true);
    //
    //
    //high dpi adjustment
    //
    stage.canvas.width = 371;
    stage.canvas.height = 742;
    //
    //
    createjs.Touch.enable(stage);
    createjs.Ticker.framerate = 24;
    //
    drawingCanvas = new createjs.Shape();
    stage.addChild(drawingCanvas);
    //stage.update();
    //
    stage.addEventListener("stagemousedown", handleMouseDown);
    stage.addEventListener("stagemouseup", handleMouseUp);

    var drawSign = new createjs.Bitmap(window.getLS('app_language') == "it" ? "resources/images/ui/draw_here_IT.svg" : "resources/images/ui/draw_here_EN.svg");
    drawSign.x = 130;
    drawSign.y = 155;
    drawSign.scaleX = 2.0;
    drawSign.scaleY = 2.3;

    stage.addChild(drawSign);
    //update the stage only when all external resources are loaded
    drawSign.image.onload = function () {
      stage.update();
    };

    function handleMouseDown(event) {
      //console.log('handle mouse event');
      if (!event.primary) {
        return;
      }
      //
      $$('.artboard-menu a').removeClass('disabled');
      //
      if (stage.contains(drawSign)) {
        stage.clear();
        //stage.removeChild(title);
        stage.removeChild(drawSign);
      }
      //
      color = window.postcard.strokecolor;
      stroke = window.postcard.strokewidth * 1.2;
      oldPt = new createjs.Point(stage.mouseX, stage.mouseY);
      oldMidPt = oldPt.clone();
      stage.addEventListener("stagemousemove", handleMouseMove);
      //console.log('handle mouse event end');
    }

    function handleMouseMove(event) {
      //console.log('handle mouse move event');
      if (!event.primary) {
        return;
      }
      var midPt = new createjs.Point(oldPt.x + stage.mouseX >> 1, oldPt.y + stage.mouseY >> 1);
      var gpx = drawingCanvas.graphics;
      gpx.clear();
      //
      //stroke style configuration
      //
      gpx.setStrokeStyle(stroke, 'round', 'round');
      //gpx.setStrokeStyle(stroke);
      //
      gpx.beginStroke(color);
      gpx.moveTo(midPt.x, midPt.y);
      gpx.curveTo(oldPt.x, oldPt.y, oldMidPt.x, oldMidPt.y);
      oldPt.x = stage.mouseX;
      oldPt.y = stage.mouseY;
      oldMidPt.x = midPt.x;
      oldMidPt.y = midPt.y;
      stage.update();
      //console.log('handle mouse move event end');
    }

    function handleMouseUp(event) {
      //console.log('handle mouse up event');
      if (!event.primary) {
        return;
      }
      stage.removeEventListener("stagemousemove", handleMouseMove);
      //console.log('handle mouse event end');
    }
  },

  resizeDrawer: function resizeDrawer() {
    var wrapper_width = this.$el.find(".postcard-editor .drawer-wrapper").width();
    var wrapper_height = this.$el.find(".postcard-editor .drawer-wrapper").height();
    var props = this.fitResizer(wrapper_width <= 900 ? wrapper_width : 900, wrapper_height <= 600 ? wrapper_height : 600, 670, 430, 2, true);
    this.$el.find(".postcard-editor .drawer-wrapper .artboard-drawer").css({
      "width": props.width + 'px',
      "height": props.height + 'px'
    });
  },

  sharePageReset: function sharePageReset() {
    var _this = this;

    console.log('sharePageReset');
    console.log(screen.orientation.type);
    setTimeout(function () {
      _this.fitArtboard();
      _this.initArtboard();
      app.sheet.open('.postcards-list');
    }, 197);
  },

  goSharePage: function goSharePage() {
    this.exportCanvas();
    var currentPath = mainView.router.currentRoute.path;
    app.router.navigate(mainView.router.currentRoute.path + 'share/');
  },
  exportCanvas: function exportCanvas() {

    var canvas = document.querySelector('.artboard-drawer canvas'),
        base64 = canvas.toDataURL();
    //console.log(base64);

    var backImage = new Image();
    backImage.src = base64;

    //window.shareCard.base64 = base64;
    window.shareCard.drawingImg = backImage;
    window.shareCard.postcardUrl = document.querySelector(".artboard-drawer img");
  },

  fitResizer: function fitResizer(containerWidth, containerHeight, ratioWidth, ratioHeight) {
    var resizeType = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 1;
    var roundProps = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : true;

    var EL_RATIO = ratioWidth / ratioHeight;
    var CONTAINER_RATIO = containerWidth / containerHeight;
    var TEST = resizeType === 2 ? EL_RATIO > CONTAINER_RATIO : EL_RATIO < CONTAINER_RATIO;

    var targetWidth = 0;
    var targetHeight = 0;

    if (TEST) {
      targetWidth = containerWidth;
      targetHeight = targetWidth / EL_RATIO;
    } else {
      targetHeight = containerHeight;
      targetWidth = targetHeight * EL_RATIO;
    }

    var result = {
      width: targetWidth,
      height: targetHeight,
      x: (containerWidth - targetWidth) / 2,
      y: (containerHeight - targetHeight) / 2
    };

    if (roundProps) {
      //res
      result.width = Math.round(result.width);
      result.height = Math.round(result.height);
      result.x = Math.round(result.x);
      result.y = Math.round(result.y);
    }

    return result;
  }

});
