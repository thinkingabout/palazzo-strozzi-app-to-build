'use strict';

define({
    postcardUrls: function postcardUrls() {
        return window.postcard.current.cardUrls;
    },
    paletteOption: function paletteOption() {
        var res = [];
        for (var i in window.postcard.colorpalette) {
            var color = window.postcard.colorpalette[i];
            res.push({ color: color, style: 'style="color:' + color + '"' });
        }
        return res;
    }
});
