"use strict";

define({
    //Lifecycle Hooks
    beforeCreate: function beforeCreate() {
        //console.log('%c[TestComponent] componentBeforeCreate', 'color: green;', this)
        window.postcardSheet = null;
    },
    created: function created() {
        //console.log('%c[TestComponent] componentCreated', 'color: green;', this)
    },
    beforeMount: function beforeMount() {
        //console.log('%c[TestComponent] componentBeforeMount', 'color: green;', this)
    },
    mounted: function mounted() {
        //console.log('%c[TestComponent] componentMounted', 'color: green;', this);
        //
        //prevent unreachable host  
        //to add in controllers
        //if(window.postcard.urls.lenght==0)mainView.router.back();
    },
    beforeDestroy: function beforeDestroy() {
        //console.log('%c[TestComponent] componentBeforeDestroy', 'color: green;', this);
    },
    destroyed: function destroyed() {
        //console.log('%c[TestComponent] componentDestroyed', 'color: green;', this);
    }
});
