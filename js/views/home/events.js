'use strict';

define({
    // Page Events

    pageMounted: function pageMounted(e, page) {
        //console.log('%c[TestComponent] pageMounted', page);
    },
    pageInit: function pageInit(e, page) {
        //console.log('%c[TestComponent] pageInit', page);
    },
    pageBeforeIn: function pageBeforeIn(e, page) {
        //console.log('%c[TestComponent] pageBeforeIn', page);
        //
        //failback from webviews
        app.preloader.hide();
        //
        mainView.router.params.stackPages = false;
        mainView.router.params.animate = true;
        //
        //
        var $nextShowsCarousel = page.$el.find('.carousel.next-shows')[0];
        if ($nextShowsCarousel) {
            //
            var nextShowsCarousel = new Flickity($nextShowsCarousel, {
                pageDots: false,
                prevNextButtons: false,
                cellAlign: 'left'
            });
            //
        }
        //
        var $nextEventsCarousel = page.$el.find('.carousel.next-events')[0];
        if ($nextEventsCarousel) {
            //
            var nextEventsCarousel = new Flickity($nextEventsCarousel, {
                pageDots: false,
                prevNextButtons: false,
                cellAlign: 'left'
            });
            //
        }
        //
    },
    pageAfterIn: function pageAfterIn(e, page) {
        //console.log('%c[TestComponent] pageAfterIn', page);
    },
    pageBeforeOut: function pageBeforeOut(e, page) {
        //console.log('%c[TestComponent] pageBeforeOut', page);
    },
    pageAfterOut: function pageAfterOut(e, page) {
        //console.log('%c[TestComponent] pageAfterOut', page);
        //
        var $nextShowsCarousel = page.$el.find('.carousel.next-shows')[0];
        if ($nextShowsCarousel) {
            //
            var nextShowsCarousel = window.Flickity(page.$el.find('.carousel.next-shows')[0]);
            nextShowsCarousel.destroy();
            //
        }
        //
        var $nextEventsCarousel = page.$el.find('.carousel.next-events')[0];
        if ($nextEventsCarousel) {
            //
            var nextEventsCarousel = window.Flickity(page.$el.find('.carousel.next-events')[0]);
            nextEventsCarousel.destroy();
            //
        }
        //     
    },
    pageBeforeRemove: function pageBeforeRemove(e, page) {
        //console.log('%c[TestComponent] pageBeforeRemove', page);
    }
});
