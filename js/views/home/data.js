"use strict";

define({

    "currentShows": function currentShows() {

        if (window.appData.content.current_shows.list.length > 0) return window.appData.content.current_shows.list;else return false;
    },
    "currentShow_multiple": function currentShow_multiple() {
        if (window.appData.content.current_shows.list.length > 1) {
            return true;
        } else return false; //window.appData.content.current_shows.list[0].webview;
    },
    "nextShows": function nextShows() {

        if (window.appData.content.next_shows.list.length > 0) return window.appData.content.next_shows.list;else return false;
    },
    "nextShows_multiple": function nextShows_multiple() {
        if (window.appData.content.next_shows.list.length > 1) {
            return true;
        } else return false; //window.appData.content.next_shows.list[0].webview;
    },
    "nextEvents": function nextEvents() {

        if (window.appData.content.next_events.list.length > 0) return window.appData.content.next_events.list;else return false;
    },
    "nextEvents_multiple": function nextEvents_multiple() {
        if (window.appData.content.next_events.list.length > 1) {
            return true;
        } else return false; // window.appData.content.next_events.list[0].webview;
    }

});
