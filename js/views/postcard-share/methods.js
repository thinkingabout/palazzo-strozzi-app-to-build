"use strict";

define({

  goNext: function goNext() {},

  goBack: function goBack() {
    this.$router.back();
  },

  postcardCanvas: function postcardCanvas() {
    var $container = this.$el.find("#sharePageCanvasWrapper");
    var canvas = $container.find("canvas.sharePage")[0];
    var stage = new createjs.Stage(canvas);

    //stage.canvas.width = $$("canvas.sharePage").width();
    stage.canvas.width = 742;
    //stage.canvas.height = $$("canvas.sharePage").height();
    stage.canvas.height = 476;

    var whiteBackground = new createjs.Shape();
    whiteBackground.graphics.beginFill("#FFFFFF").drawRect(0, 0, stage.canvas.width, stage.canvas.height);
    stage.addChild(whiteBackground);

    var front = new createjs.Bitmap(window.shareCard.postcardUrl.getAttribute('src'));
    //console.log('front-size: ' + front.image.width + ' ' + front.image.height);
    front.scaleX = stage.canvas.width / 2 / front.image.width;
    front.scaleY = stage.canvas.height / front.image.height;
    front.x = 0;
    stage.addChild(front);

    var back = new createjs.Bitmap(window.shareCard.drawingImg);
    //console.log('back-size: ' + back.image.width + ' ' + back.image.height);
    back.scaleX = stage.canvas.width / 2 / back.image.width;
    back.scaleY = stage.canvas.height / back.image.height;
    back.x = stage.canvas.width / 2;
    stage.addChild(back);

    stage.update();
    stage.update();
  },

  resizeDrawer: function resizeDrawer() {
    //console.log('resizing...');
    var wrapper_width = $$("body").width() - 45;
    //console.log(wrapper_width);
    var wrapper_height = $$("body").height() - 45;
    //console.log(wrapper_height);
    var props = this.fitResizer(wrapper_width <= 900 ? wrapper_width : 900, wrapper_height <= 600 ? wrapper_height : 600, 670, 430, 2, true);
    //console.log(props);
    $$("#sharePageCanvasWrapper").css({
      "width": props.width + 'px',
      "height": props.height + 'px'
    });
    //console.log('resized')
  },

  fitResizer: function fitResizer(containerWidth, containerHeight, ratioWidth, ratioHeight) {
    var resizeType = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 1;
    var roundProps = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : true;

    var EL_RATIO = ratioWidth / ratioHeight;
    var CONTAINER_RATIO = containerWidth / containerHeight;
    var TEST = resizeType === 2 ? EL_RATIO > CONTAINER_RATIO : EL_RATIO < CONTAINER_RATIO;

    var targetWidth = 0;
    var targetHeight = 0;

    if (TEST) {
      targetWidth = containerWidth;
      targetHeight = targetWidth / EL_RATIO;
    } else {
      targetHeight = containerHeight;
      targetWidth = targetHeight * EL_RATIO;
    }

    var result = {
      width: targetWidth,
      height: targetHeight,
      x: (containerWidth - targetWidth) / 2,
      y: (containerHeight - targetHeight) / 2
    };

    if (roundProps) {
      //res
      result.width = Math.round(result.width);
      result.height = Math.round(result.height);
      result.x = Math.round(result.x);
      result.y = Math.round(result.y);
    }

    return result;
  },

  sharePostcard: function sharePostcard() {
    var canvas = document.querySelector("canvas.sharePage");
    var base64 = canvas.toDataURL();

    var name = $$("#showName").text();

    var options = {
      subject: name,
      files: [base64]
    };

    var onSuccess = function onSuccess(result) {
      console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
      console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
    };

    var onError = function onError(msg) {
      console.log("Sharing failed with message: " + msg);
    };

    window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
  },

  resetArtboard: function resetArtboard() {

    //let href = this.$el.find('a.link.postcardComposer')[0].dataset.href;
    $$('body').css("opacity", 0);

    if (Framework7.device.phonegap) {
      window.handleNavigate = function (e) {
        console.log('handleNavigateFromPS');
        $$('.page[data-page="postcard-composer"]')[0].f7Component.sharePageReset();
        mainView.router.back();
      };
      //Needs to be removed??
      screen.orientation.addEventListener('change', handleNavigate);
      window.screen.orientation.lock('landscape');
    } else {
      $$('.page[data-page="postcard-composer"]')[0].f7Component.sharePageReset();
      mainView.router.back();
    }
  }

});
