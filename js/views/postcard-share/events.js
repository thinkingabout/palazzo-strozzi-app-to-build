"use strict";

define({
    pageMounted: function pageMounted(e, page) {
        //console.log('%c[TestComponent] pageMounted', page);
        //
        //if(Framework7.device.phonegap) window.screen.orientation.lock('portrait');
        //
    },
    pageInit: function pageInit(e, page) {
        //console.log('%c[TestComponent] pageInit', page);
        app.preloader.show();
        $$(".preloader-backdrop").addClass("alt1");
    },
    pageBeforeIn: function pageBeforeIn(e, page) {
        //console.log('%c[TestComponent] pageBeforeIn', page);
        //
        if (Framework7.device.phonegap) window.screen.orientation.lock('portrait');
        //
        $$('#showName').text(window.postcard.current.name);
        this.resizeDrawer();
        this.postcardCanvas();
        window.tmpResizeEvent2 = window.addEventListener("resize", function () {
            if (mainView.router.currentRoute.name != "Postcard Share") return;
            console.log('resizing from e listener(postcardshare)...');
            var com = mainView.router.currentPageEl.f7Component;
            com.resizeDrawer();
        });
    },
    pageAfterIn: function pageAfterIn(e, page) {
        //console.log('%c[TestComponent] pageAfterIn', page);
        this.postcardCanvas();
        $$('#sharePageCanvasWrapper').css("opacity", "1");
        app.preloader.hide();
    },
    pageBeforeOut: function pageBeforeOut(e, page) {
        //console.log('%c[TestComponent] pageBeforeOut', page);
        //
        if (Framework7.device.phonegap) window.screen.orientation.lock('landscape');
        //
        window.tmpResizeEvent2 = null;
        //

        //
    },
    pageAfterOut: function pageAfterOut(e, page) {
        //console.log('%c[TestComponent] pageAfterOut', page);
    },
    pageBeforeRemove: function pageBeforeRemove(e, page) {
        //console.log('%c[TestComponent] pageBeforeRemove', page);
    }
});
