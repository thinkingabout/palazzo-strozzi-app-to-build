'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function (window) {

    //

    var Routes = function () {
        function Routes(routeList) {
            _classCallCheck(this, Routes);

            this.init();
        }
        /**
         *  templatePath (string)
         *  opt.data = {} || opt.dataPath
         * 
         */


        _createClass(Routes, [{
            key: 'pushComponentPage',
            value: function pushComponentPage(opt) {
                var animate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

                this.routeList.push({
                    name: opt.name,
                    path: opt.path,
                    options: {
                        animate: animate
                    },
                    async: function async(routeTo, routeFrom, resolve, reject) {
                        console.log('loading async __ before require');
                        require(['!domReady', 'text!resources/' + opt.templatePath, opt.dataPath, opt.eventsPath, opt.hooksPath, opt.methodsPath], function (doc, template, data, events, hooks, methods) {
                            //
                            var com = { component: { 'template': template } };
                            //
                            if (opt.data) com.component['data'] = function () {
                                return opt.data;
                            };
                            if (opt.dataPath) com.component['data'] = function () {
                                return data;
                            };
                            //
                            if (opt.eventsPath) com.component['on'] = events;
                            //
                            if (opt.hooksPath) Object.assign(com, hooks);
                            //
                            if (opt.methodsPath) com.component['methods'] = methods;
                            //
                            console.log('loading async __ in require -> resolving');
                            //console.log(':::::DEBUGGIN COM:::::');
                            console.log(opt.name);
                            console.log(opt.path);
                            //console.log(com);
                            //console.log(':::::DEBUGGIN COM END:::::');
                            resolve(com);
                            //
                        }, function (err) {
                            //console.log(':::::DEBUGGIN ERRORS:::::');
                            console.log('loading async __ in requrire __ printing errors');
                            console.log(err);
                            //console.log(':::::DEBUGGIN END:::::');
                        });
                    }
                });
                //this.refreshRoutes();
            }
        }, {
            key: 'refreshRoutes',
            value: function refreshRoutes() {
                window.mainView.routes = this.routeList;
            }
        }, {
            key: 'getRoutes',
            value: function getRoutes() {
                return this.routeList;
            }
        }, {
            key: 'pushWebView',
            value: function pushWebView(path, label, dataName) {
                var opt = {
                    name: label,
                    path: '/info/' + path + '/',
                    templatePath: 'components/webview.html',
                    data: {
                        "title": label,
                        "startingUrl": "",
                        "url": function url() {
                            return window.appData.content.info[dataName];
                        }
                    },
                    //hooksPath: 'js/components/webview/hooks',
                    eventsPath: 'js/components/webview/events'
                };
                this.pushComponentPage(opt);
            }
        }, {
            key: 'init',
            value: function init() {
                this.routeList = [
                //ROOT
                {
                    path: "/",
                    options: {
                        animate: false
                    },
                    on: {
                        pageBeforeIn: function pageBeforeIn(e, page) {
                            app.router.navigate('/home/');
                        },
                        pageAfterIn: function pageAfterIn(e, page) {
                            app.router.navigate('/home/');
                        }
                    }
                },
                ///CURRENT SHOWS
                {
                    path: "/current-shows/",
                    //componentUrl: "./resources/pages/shows/list.html"
                    componentUrl: "./resources/pages/shows/list-cards.html"
                }, {
                    name: "Current Show",
                    path: "/current-shows/:showId",
                    componentUrl: "./resources/pages/shows/show.html"
                },
                ///SPECIAL CONTENTS
                {
                    path: "/current-shows/:showId/special-content/",
                    componentUrl: "./resources/pages/specials/special-content.html"
                },
                ///NEXT SHOWS
                {
                    path: "/next-shows/",
                    //componentUrl: "./resources/pages/next-shows/list.html"
                    componentUrl: "./resources/pages/next-shows/list-cards.html"
                }, {
                    path: "/next-show/:showId",
                    componentUrl: "./resources/pages/next-shows/show.html"
                },
                ///NEXT EVENTS
                {
                    path: "/next-events/",
                    //componentUrl: "./resources/pages/events/list.html"
                    componentUrl: "./resources/pages/events/list-cards.html"
                }, {
                    path: "/next-event/:eventId",
                    componentUrl: "./resources/pages/events/show.html"
                },
                //Connection Check Overlay
                {
                    path: "/check/",
                    name: "Connection Check",
                    options: {
                        animate: false
                        //pushState:false <-does it works?
                    },
                    componentUrl: "./resources/components/connection-check.html"
                    //404
                    //{
                    //    path: '*',
                    //    //url: './pages/404.html',
                    //    componentUrl: "./resources/pages/home.html",
                    //},
                }];
                //home
                this.pushComponentPage({
                    name: 'Home',
                    path: '/home/',
                    templatePath: 'pages/home.html',
                    dataPath: 'js/views/home/data',
                    hooksPath: 'js/views/home/hooks',
                    eventsPath: 'js/views/home/events'
                }, false);

                //postcard-composer
                this.pushComponentPage({
                    name: 'Postcard Composer',
                    path: '/current-shows/:showId/postcard-composer/',
                    templatePath: 'pages/specials/postcard-composer.html',
                    dataPath: 'js/views/postcard-composer/data',
                    hooksPath: 'js/views/postcard-composer/hooks',
                    eventsPath: 'js/views/postcard-composer/events',
                    methodsPath: 'js/views/postcard-composer/methods'
                }, false);

                //postcard-share
                this.pushComponentPage({
                    name: 'Postcard Share',
                    path: '/current-shows/:showId/postcard-composer/share/',
                    templatePath: 'pages/specials/postcard-share.html',
                    //dataPath: 'js/views/postcard-share/data',
                    //hooksPath: 'js/views/postcard-share/hooks',
                    eventsPath: 'js/views/postcard-share/events',
                    methodsPath: 'js/views/postcard-share/methods'
                }, false);
                //this.loadWebViews();
            }
        }, {
            key: 'loadWebViews',
            value: function loadWebViews() {
                //let info = window.appData.content.info;
                var links = [['opening-hours', 'opening_hours', "opening_hours_webview"], ['find', 'find', "find_webview"], ['faq', 'faq_webview', "faq_webview"], ['history', 'history', "history_webview"], ['foundation', 'foundation', "foundation_webview"], ['visits', 'visit', "visits_webview"], ['tickets', 'tickets', "tickets_webview"]];
                for (var i in links) {
                    this.pushWebView(links[i][0], links[i][1], links[i][2]);
                }
            }
        }, {
            key: 'reload',
            value: function reload() {
                this.routeList = null;
                this.init();
                this.loadWebViews();
                window.mainView.routes = this.routeList;
            }
        }]);

        return Routes;
    }();

    window.appRoutes = new Routes();
})(window);
