(function (window) {

    class ObjectFitResizer {

        constructor() {
            //
        }

        static get RESIZE_TYPE_COVER() {
            return 1;
        }

        static get RESIZE_TYPE_CONTAIN() {
            return 2;
        }

        static fit(containerWidth, containerHeight, ratioWidth, ratioHeight, resizeType = window.ObjectFitResizer.RESIZE_TYPE_COVER, roundProps = true) {
            const EL_RATIO = ratioWidth / ratioHeight;
            const CONTAINER_RATIO = containerWidth / containerHeight;
            const TEST = (resizeType === ObjectFitResizer.RESIZE_TYPE_CONTAIN) ? (EL_RATIO > CONTAINER_RATIO) : (EL_RATIO < CONTAINER_RATIO);

            let targetWidth = 0;
            let targetHeight = 0;

            if (TEST) {
                targetWidth = containerWidth;
                targetHeight = targetWidth / EL_RATIO;
            } else {
                targetHeight = containerHeight;
                targetWidth = targetHeight * EL_RATIO;
            }

            let result = {
                width: targetWidth,
                height: targetHeight,
                x: (containerWidth - targetWidth) / 2,
                y: (containerHeight - targetHeight) / 2
            };

            if (roundProps) {
            	//res
                result.width = Math.round(result.width);
                result.height = Math.round(result.height);
                result.x = Math.round(result.x);
                result.y = Math.round(result.y);
            }

            return result;
        }
    }

    window.ObjectFitResizer = ObjectFitResizer;
})(window);